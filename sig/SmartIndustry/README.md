# 面向工业物联场景的泛在操作系统 (SIG)

面向工业领域多节点设备的工业物联泛在操作系统，其部署主要包括节点端的多节点集群和边缘端/云端的计算设施，如图 1所示:
![工业物联泛在操作系统部署环境示意图](https://xuos.io/images/xiuos-deploy.png) 

在节点端，支持多种工厂环境传感设备，既包括基础的环境传感器感知温湿度、烟雾气体、噪音等，也包括智能传感器可对人体、物体、声音进行识别，这些传感器支持多种网络通信互联协议，可以按需自组网络，通过网关设备向外交换数据，实现智能物联框架的“感、联”功能。网关设备对同一个集群中的节点设备进行管理，负责和边缘/云端系统交换数据，并可具备有限的“知、控”功能。

节点端感知的数据经网关传输给边缘/云端系统之后，边缘/云端的工业互联网平台可对汇聚而来的数据进行分析，将有价值的数据在控制中心的大屏、管理人员的移动设备上进行展示，并根据工业应用的业务规则，通知和调度“设备、排程、人员”，对生产过程进行优化，完成“知、控”。节点端的内部体系结构如图 2所示，主要包括硬件层、系统层、框架层。在此基础上，再结合云边协同，共同构建应用层。
![工业物联泛在操作系统系统架构图](https://xuos.io/images/xiuos-arch.png)

## 工作目标

IndustrialUOS的目标是通过工业物联网的部署和应用，促进工业领域人、机、物的深度互联，促进工厂的数字化转型升级，使能智能化工业生产新体系。要实现该目标，工业物联网操作系统需要解决“感、联、知、控“四方面的问题:

- 感: 复杂多样的工业生产实体智能地识别、感知和采集生产相关数据;
- 联: 工业数据在互联互通的网络上进行传输和汇聚;
- 知: 对这些网络化的工业大数据形成快速处理和实效分析;
- 控: 将数据分析所得到的信息反馈到工业生产中，对生产活动进行改善。

## SIG 成员

### Owner

- zhaobozhang (zhang_zb@pku.edu.cn)

### Maintainers

- zhaobozhang (zhang_zb@pku.edu.cn)

## SIG 维护包列表

- 
- 

## SIG 邮件列表

- 

## SIG 组例会

月会
