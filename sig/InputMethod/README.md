## 输入法SIG组

### 工作目标

&emsp;&emsp;输入法SIG组是由一群热爱开源技术的爱好者所组成的团队。我们致力于为开源操作系统开发高质量的输入法，并在开源社区中积极推广和分享相关技术。   
&emsp;&emsp; SIG组欢迎广大开源爱好者积极参与进来，为开源输入法框架和开源输入法的发展和完善贡献自己的力量。同时，SIG组也将不断探索和尝试新的技术，为用户提供更加便捷、高效的输入体验。

### SIG成员

| SIG成员                                                       | 邮箱                                                        |
| --------------------------------------------------------------| ----------------------------------------------------------- |
| 韩腾[@hantengc](https://gitee.com/hantengc)                   | [hanteng@kylinos.cn](mailto:hanteng@kylinos.cn)             |
| 刘林松[@kingysu](https://gitee.com/kingysu)                   | [liulinsong@kylinos.cn](mailto:liulinsong@kylinos.cn)       |
| 林煜烜[@linyuxuanlinkun](https://gitee.com/linyuxuanlinkun)   | [2909333157@qq.com](mailto:2909333157@qq.com)               |
| 杜志民[@freeime](https://gitee.com/freeime)                   | 1361133xxxx@163.com      |
| 张鹏飞[@iaom](https://gitee.com/iaom)                         | [zhangpengfei@kylinos.cn](mailto:zhangpengfei@kylinos.cn)   |

### SIG维护包列表
- fcitx
- fcitx-qt5
- fcitx-configtool
- im-config
- fcitx5
- fcitx5-qt
- fcitx5-gtk
- fcitx5-configtool
- fcitx5-chinese-addons
- fcitx5-rime
- kylin-virtual-keyboard 
- fcitx5-quwei
- fcitx5-gb18030
- librime
- brise
- ok-input-method

