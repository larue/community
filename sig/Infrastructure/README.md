# 基础设施 SIG

负责openKylin社区的基础平台系统功能的开发、维护。

## SIG 职责和目标

- 负责CLA平台的开发、维护工作
- 负责CCBS平台的开发、维护工作
- 负责CCIF平台的开发、维护工作
- 负责CCCI平台的开发、维护工作
- 负责https://gitee.com/openkylin 社区基础设施开发维护

## 仓库
- [基础设施issue提交](https://gitee.com/openkylin/infrastructure-management/issues)
- live-build (镜像构建工具)
- debootstrap (chroot构建工具)
- okbs-tools (OKBS API调用)
- openkylin-keyring (openkylin官方仓库签名)
- openkylin-archive-anything (补充PPA仓库签名)

## SIG 成员
### Owner
- 魏立峰

### 维护者
- maozhou
- xiewei
- jiangwei124

## 邮件列表
infrastructure@lists.openkylin.top
