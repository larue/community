# Avalonia SIG

Avalonia UI 是一个基于.NET 和 XAML 的跨平台 UI 开发框架。Avalonia SIG 致力于推荐 Avalonia UI 对 openKylin 生态的支持，及 Avalonia UI 的相关生态建设。

## 工作目标

- 提供 Avalonia UI 在麒麟系列操作系统开发、部署、运行的最佳实践。
- 完善 Avalonia UI 对 openKylin 在系统交互层面的支持。
- 提供开箱即用的符合 UKUI 设计规范的 Avalonia UI 主题风格。
- 依托 Avalonia UI 中文社区为 Avalonia UI 开发者提供完善的中文文档。

## SIG 成员

### Owner

董彬[@rabbitism](https://gitee.com/rabbitism)([dongbin@irihi.tech](mailto:dongbin@irihi.tech))

### Maintainer

NLNet[@NLnet](https://gitee.com/nlnet)([yangqi1990917@163.com](mailto:yangqi1990917@163.com))

张典[@zdpcdt](https://gitee.com/zdpcdt)([dian@irihi.tech](mailto:dian@irihi.tech))
