
## SIG说明

- 青霜Web引擎/开发框架参考借鉴MiniBlink工程思想，以Blink排版引擎为基础，采用“加法思路”，自研、改写或完善其他必要的工程组件。目前以Chromium116版本为基础，主要使用了Blink、V8、Skia等组件，同时也参考了FireFox、Webkit等开源项目。

- 其中Skia组件由唐远炎教授（澳门大学首席教授、IEEE终身院士、IAPR院士、AAIA院士）领衔博士后团队，针对信创及现代多媒体复杂应用场景，对底层图形核心绘制算法做了深度改造及优化，后续我们计划推出一套自己的2D图形库。

- 项目取名至《滕王阁序》中“紫电青霜，王将军之武库”，青霜是古代名剑的名字，目前为国内信创领域内唯一自研的Web引擎/开发框架，采用微内核+插件化架构，以同类竞品10%的体积实现了其90%的Web能力。

- 项目获得中科院软件所及工信部信发司大力支持，致力于成为国产信创领域内首款、安全、可信、可控、可维护的Web引擎/开发框架。


## SIG目标
经孵化成熟后，作为麒麟系统的基础组件，集成进发行版中。


## 项目介绍
- 重度孵化 —— 中科院软件所及工信部信发司重点孵化项目，是其标杆创业项目
- 技术验证 —— 客户端运行总数超一亿台；
- 技术形态 —— 1) 国内最极致的微内核MicroKernel架构；2) 插件化；3) 智能伸缩Web引擎；4) 分布式Web引擎设计
- 效果指标 —— 1) 体积：1/10；2) 性能：持平；3) Web兼容度：90%；4) 目前为全球最轻量web引擎
- 官网 —— http://www.rooooot.com/


## 代码仓库
- https://gitee.com/openkylin/qsframework


## SIG管理员
- [@ampereufo](https://gitee.com/ampereufo)


## SIG成员
- [@ampereufo](https://gitee.com/ampereufo)
- [@pengzhe](https://gitee.com/pengzhe)
- [@bugcheck](https://gitee.com/bugcheck)
- [@yuan-jie](https://gitee.com/yuan-jie)


## SIG邮件列表
- qsframework@lists.openkylin.top