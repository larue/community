# GamesAdaptation兴趣小组（SIG）

- GamesAdaptation SIG组致力于为openKylin社区适配可用的游戏框架及开发原生游戏，为社区开发者和爱好者提供丰富的游戏开发环境以及游戏应用，促进openKylin游戏生态的发展。

## 工作目标

- 1、主流游戏框架的适配与移植;
- 2、Linux原生游戏的开发与打包；
- 3、链接社区广大游戏爱好者，促进游戏生态的发展。


## 维护包列表

- Archer(412666227@qq.com)

## SIG 成员

### Owner

- Archer(412666227@qq.com)

### Maintainers

- Archer(412666227@qq.com)

## 邮件列表

- [gamesadaptation@lists.openkylin.top]