## 媒体推广组(MediaPromotion SIG)
### 工作目标:
#### MediaPromotion SIG组致力于在各媒体渠道推广社区优质内容，包括宣传视频、技术文档、活动招募等内容：
-  1、负责自媒体平台，如B站、视频号、抖音号、CSDN等平台上优质内容推广宣传，技术文档输出，活动宣发等；
-  2、负责引流平台优质粉丝到社区，并转化为社区爱好者或开发者；
-  3、负责跟进与分析平台推广效果，反馈并优化推广内容质量。

## SIG成员:
### Owner
- yuhypython

### Maintainers
- yuhypython
- 果冻Swiftie（https://gitee.com/jelly-swiftie）
- DSOE1024（https://gitee.com/DSOE1024）

## SIG邮件列表:
- mediapromotion@lists.openkylin.top
