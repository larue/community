# VUP特别兴趣小组

## 工作目标

虚拟UP博主（Virtual Uploader，简写VUP）SIG致力于以下具体工作方向

- 社区活动新闻宣传、Linux技术科普、观众互动直播、国内外科技热点互动
- 建立和运营openKylin社区旗下VUP——小K
- 负责小K形象Q版设计和推广，表情包和周边物料设计内容输出，推出社区二次元品牌形象

## 成员

### Owner
- [映月澜清](https://gitee.com/Fic_M_myshadowandsoul)

### Maintainers

- [楠瑾](https://gitee.com/id-nanjin)
- [DSOE1024](https://gitee.com/DSOE1024)
- [kangyanhong](https://gitee.com/kang_yan_hong)
- [爱吃西红柿](https://gitee.com/wei-yunbo)
- [果冻Swiftie](https://gitee.com/jelly-swiftie)
- [小鹿](https://gitee.com/francxixi)
-[映月澜清](https://gitee.com/Fic_M_myshadowandsoul)
