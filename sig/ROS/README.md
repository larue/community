# ROS SIG

ROS SIG 致力于负责 ROS/ROS2 开源软件包在 openKylin 上的移植，发布和维护。发布基于 openKylin 的 ROS/ROS2 版本，并进行 ROS/ROS2 软件包打包、BUG 修复等工作。

## 工作目标

  1. 负责 ROS/ROS2 对 openKylin 的适配和移植。
  2. 负责后续 ROS/ROS2 在 openKylin 的软件包更新和维护。
  3. 基于 openKylin 和 ROS/ROS2 组合开源的 ROS 演示方案。

## 邮件列表

ros@lists.openkylin.top

## SIG成员

### Owner

- [junqiang_wang](https://gitee.com/geasscore)
- [cheng_li](https://gitee.com/shiptux)

### Maintainers

- [xiao_yun_wang](https://gitee.com/xiao_yun_wang)
- [ryan](https://gitee.com/c116)
- [weiwei_li](https://gitee.com/angell1518)
- [min_sun](https://gitee.com/sunmin89)
- [wei_wu](https://gitee.com/wuwei_plct)

### Contributors



## 仓库列表

- 待创建
