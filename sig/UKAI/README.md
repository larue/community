# UKAI兴趣小组
## 职责和目标
优凯（UKAI）:统一的麒麟人工智能(Universal Kylin Artificial Intelligence)，一种智能化的交互系统，致力于打造基于linux操作系统的“Windows Copilot”

## SIG成员
### SIG-owner
李荣振(`lirongzhen@cqu.edu.cn`)
### SIG-maintainer
钟将(`zhongjiang@cqu.edu.cn`)
戴启祝(`daiqizhu@cqu.edu.cn`)
朱伟(`zhuwei01@cqu.edu.cn`)

## SIG维护包列表
- [aiagent](https://gitee.com/openkylin/aiagent)

## SIG邮件列表
> ukai@lists.openkylin.top

## SIG组例会
> 月会