## Godot SIG 

Godot是世界上最流行的开源游戏引擎之一。Godot SIG致力于发展Godot中国社区，为开源事业添砖加瓦！  

## SIG成员

### Owner

- [沫萝](https://gitee.com/moluopro)  

### Maintainers
 
- [熊猫](https://gitee.com/MagickPanda)  

- [pig](https://gitee.com/Ride_A_Pig)

## 工作方向

1. 维护openKylin系统源里的Godot引擎及其依赖。
2. 引导、帮助社区成员完成自己的作品，鼓励他们开源出来。
2. 不定期举办社区活动，进行线下会面，持续扩大社区影响力。  

## 加入我们

如果您是Godot开发者并愿意参与开源工作，欢迎加入我们！