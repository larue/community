# FAQ SIG

## 简介

FAQ SIG（FAQ特别兴趣小组）负责帮助社区用户解决问题，推动搭建由AI和数据库驱动的openKylin社区问题解决工作流，同时推动建立社区知识库。


## 工作目标

收集各渠道开发者、爱好者等用户反馈的问题，并建立相关标准化流程推动问题解答或解决同时，在这一过程中不断为 openKylin 社区积累FAQ知识库，同时与各个SIG组的开发工作保持密切合作并参与到issue管理，在例会进行issue指派和里程碑讨论。

1、标准化问题收集、推动解决、结果反馈等整个流程。

2、与文档组合作搭建社区知识库，形成标准化问题答疑或操作手册。

3、宣传社区已搭建的平台，培养用户主动使用各种平台的习惯。

4、issue提交、管理和跟进，指派和里程碑。

5、自动化问题收集，问题数据库整理。

6、论坛问题反馈板块问题收集和答疑。

## 维护仓库

[FAQ Bot](https://gitee.com/openkylin/faq-bot)

[FAQ Bot Weixin](https://gitee.com/openkylin/faq-bot-weixin)

[FAQ Bot Forum](https://gitee.com/openkylin/faq-bot-forum)

[SIG规范](https://gitee.com/openkylin/faq-specification)

[SIG文档](https://gitee.com/openkylin/faq-docs)

[软件上架需求](https://gitee.com/openkylin/listing-requirements)


## SIG成员

### Owner

- DSOE1024 (https://gitee.com/DSOE1024)

### Maintainers

- zhangtianxiong (https://gitee.com/kiber)
- kangyanhong (https://gitee.com/kang_yan_hong)
- 江同学 (https://gitee.com/JiangZLY)
- 沧海人间（https://gitee.com/the-sea-and-the-world）
- 爱吃西红柿（https://gitee.com/wei-yunbo）
- trackme（https://gitee.com/trackme）
- fslong（https://gitee.com/fensl）
- 果冻Swiftie（https://gitee.com/jelly-swiftie）
- 许九卿 (https://gitee.com/xu-jiuqing)

## 工作流程

![输入图片说明](工作流程.png)