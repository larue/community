## UKUI基础应用SIG
### 工作目标
致力于openKylin社区的UKUI基础应用开发和维护，扩展openKylin系统的生态

## SIG成员
### Owner
- 李鑫（lixin@kylinos.cn）

### Maintainers
- 李鑫 (lixin@kylinos.cn)
- 姜丁源 (jiangdingyuan@kylinos.cn)
- 范昱辰(fanyuchen@kylinos.cn)
- 邹俊男 (zoujunnan@kylinos.cn)

### Package Maintainer
- yhkylin-backup-tools
  + 赵民勇（zhaominyong@kylinos.cn）
- ukui-notebook
  + 姜丁源 (jiangdingyuan@kylinos.cn)

## SIG维护包列表
- kylin-printer
- kylin-scanner
- kylin-recorder
- kylin-photo-viewer
- kylin-calculator
- kylin-ipmsg
- kylin-weather

## SIG邮件列表
- ukuiapplications@list.openkylin.top